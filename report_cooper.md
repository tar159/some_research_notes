Data Analysis
================
T. Ruzmetov
July 17 , 2017

Introduction
============

Here I performe transition state analysis of combined multiple long equilibrium simulations at Tm for IDP binding to its target. By calculating cross correlation on local contact formation for each residue we can demonstrate how sequential binding and unbinding take place.

``` r
#Loading all necessary libraries
library("ggplot2")       #|> for plotting 
library("lattice")       #|> req. by other packs
library("plyr")          #|> req. by other packs
library("Rmisc")         #|> for combining plots
library("corrplot")      #|> for correlation matrix calc. and plot  
library("reshape2")      #|> for data.frame manipulation
library("latex2exp")     #|> for latex
```

``` r
#loading the data
input_fast <- read.table("data/fast")
input_slow <- read.table("data/slow")

#assigning a.a. names by only index
col_names <- c('q_int', as.character(seq(119,146)))

col_needed <- c('q_int',
               '120','121','124','125',
               '127','128','131','132','133',
               '134','135','137','138','139','140',
               '141','142','143','144','145','146')

names(input_fast) <- col_names
names(input_slow) <- col_names

# get rid of non-native contacts by choosing only native
input_fast <- input_fast[,col_needed]
input_slow <- input_slow[,col_needed]

q_max <- 77             #|> tot native intermol. contacts
lower_lim <- 0.01*q_max #|> transition state lower lim.
upper_lim <- 1.0*q_max  #|> transition state upper lim.

# extracting transition state data
trans_fast <- input_fast[input_fast$q_int > lower_lim & input_fast$q_int <= upper_lim,]
trans_slow <- input_slow[input_slow$q_int > lower_lim & input_slow$q_int <= upper_lim,]
```

``` r
# dropping q_int
trans_fast$q_int <- NULL  
trans_slow$q_int <- NULL  

# evaluating corr.matrix
corr_matrix_fast <- cor(trans_fast, use = "everything")
corr_matrix_slow <- cor(trans_slow, use = "everything")

# setting NA's to zero
corr_matrix_fast[is.na(corr_matrix_fast)] <- 0
corr_matrix_slow[is.na(corr_matrix_slow)] <- 0
```

``` r
par(mfrow=c(1,2), font=2, font.main=10, cex=0.8)
corrplot(corr_matrix_fast, type = "lower", tl.col="black", tl.cex = .8)
title(main = "fast", adj = 0.3)
corrplot(corr_matrix_slow, type = "lower", tl.col="black", tl.cex = .8)
title(main = "slow", adj = 0.3)
```

![](report_cooper_files/figure-markdown_github/unnamed-chunk-4-1.png)

Correlation plots for transition region show no difference between fast and slow reconfigurations, which suggests that difference might be only in kinetics!

Let's see how correlations decay in chain length
------------------------------------------------

First, we make a function to calculate mean over lover diagonals of our correlation matrix and store values in a list, so we can later plot it against chain length.

``` r
MeanLowerDiag <- function(input_mat, chain_length){
   
    cols <- head(tail(colnames(input_mat),chain_length), chain_length - 1)
    rows <- head(tail(rownames(input_mat),chain_length), chain_length - 1)
    
    
    a <- 0                                       #initilizing empty vec.
    i <- 0
    Nrows <- length(rows)
    
    #In order to calc. mean of lower diag's of matrix, we subset square matrix
    #iteratively by cutting off 1st row and the last column untill size reduces
    #to 2x2
    while ( Nrows >= 2){                        #loop stops when mat_size is less than 2
        i <- i + 1
        a[i] <- mean(diag(input_mat[cols,rows]))#calc. mean of diag elements
        cols <- cols[-1]                        #cut off 1st col        
        rows <- rows[-length(rows)]             #cut off last row
        Nrows <- length(rows)                   #recalculate Nrows
    }

    return(a)                                   #return a vector
}
```

Now, let's apply this function to both matricies to get average over lower dioganal elements.

``` r
tail_length <- 21   
x <- MeanLowerDiag(corr_matrix_fast, tail_length)
y <- MeanLowerDiag(corr_matrix_slow, tail_length)
df <- data.frame(array(0:(length(x)-1)),x,y)
colnames(df) <- c("steps","corr_fast","corr_slow")
head(df,3)
```

    ##   steps corr_fast corr_slow
    ## 1     0 1.0000000 1.0000000
    ## 2     1 0.6220604 0.6523522
    ## 3     2 0.5015662 0.5379276

Now we can plot average correlation vs chain length.

``` r
par_fast <- 0.2
par_slow <- 0.2
library(extrafont)
```

    ## Registering fonts with R

``` r
p1 <- ggplot(df,aes(x=steps, y=corr_fast )) +
          geom_point(colour="red") +
          geom_line(colour="red") +
          geom_smooth(method="lm",
                      formula = (y ~ exp(-par_fast*x)),
                      se=FALSE,
                      linetype = 1) +
          theme_bw() +
          ylim(0.0, 1.) +
          xlim(0.0,tail_length -1) +
          labs(title = "fast", x = TeX('$\\Delta$'), y = "P", color="") +
          theme(legend.position = c(0.6,0.9),
                axis.title = element_text(size = 16.0),
                axis.text = element_text(size=10, face = "bold"), 
                plot.title = element_text(size = 15, hjust = 0.5),
                text = element_text(family="Times New Roman"),
                axis.text.x = element_text(colour="black"),
                axis.text.y = element_text(colour="black"))

p2 <- ggplot(df,aes(x=steps, y=corr_slow )) +
          geom_point(colour="red") +
          geom_line(colour="red") +
          geom_smooth(method="lm",
                      formula = (y ~ exp(-par_slow*x)),
                      se = FALSE,
                      linetype = 1) +
          theme_bw() +
          ylim(0.0, 1.) +
          xlim(0,tail_length -1) +
          labs(title = "slow", x = TeX('$\\Delta$'),y="", color="") +
          theme(legend.position = "none",
                axis.title = element_text(size = 15.0),
                axis.text = element_text(size=10, face = "bold"), 
                plot.title = element_text(size = 16, hjust = 0.5),
                text = element_text(family="Times New Roman"),
                axis.text.x = element_text(colour="black"),
                axis.text.y = element_text(colour="black"))

suppressWarnings(multiplot(p1, p2, cols=2))
```

![](report_cooper_files/figure-markdown_github/unnamed-chunk-7-1.png)

Above plots show a marginal difference in correlation decay, where IDP with slow reconfiguration show negligably longer characteristic length. This is not conclusive yet, and main target of data analysis for better comparison should be kinetics output rather than thermodynamic result.
